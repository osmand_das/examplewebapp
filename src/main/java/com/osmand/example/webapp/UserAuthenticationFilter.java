package com.osmand.example.webapp;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UserAuthenticationFilter implements Filter {

	@Override
	public void destroy() {
		System.out.println("destroying filter...");
		
	}

	@Override
	public void doFilter(ServletRequest servletReq, ServletResponse servletResp, FilterChain filterChain)
			throws IOException, ServletException {
		
		System.out.println("in doFiler() ...");
		
		HttpServletRequest req = (HttpServletRequest)servletReq;
		HttpServletResponse resp = (HttpServletResponse)servletResp;
		
		long start = System.currentTimeMillis();
				
		boolean isBadLogin = false;
		String user = null;
		String pathInfo = null;
		
		HttpSession session = req.getSession(false);
		System.out.println("===========> HttpSession: " + session);
		System.out.println("isRequestedSessionIdValid: " + req.isRequestedSessionIdValid());
		System.out.println("isRequestedSessionIdFromCookie: " + req.isRequestedSessionIdFromCookie());
		
		System.out.println("\n\n");		
		System.out.println("Cookie info");
		
		Cookie[] cookieArray = req.getCookies();
		if(cookieArray != null){
			for(int i=0; i< cookieArray.length; ++i){
				Cookie c = cookieArray[i];
				System.out.println(c.getName() + " = " + c.getValue());
			}
		}
		
		System.out.println("\n\n");
		System.out.println("Header info");
		
		Enumeration enumeration = req.getHeaderNames();
		if(enumeration != null){
			while(enumeration.hasMoreElements()){
				String headerName = (String)enumeration.nextElement();
				System.out.println(headerName + " = " + req.getHeader(headerName));
			}
		}
		
		if(session != null) {
			System.out.println("getCreationTime: " + session.getCreationTime());
			System.out.println("getId: " + session.getId());
			System.out.println("getLastAccessedTime: " + session.getLastAccessedTime());
			System.out.println("getMaxInactiveInterval: " + session.getMaxInactiveInterval());
			System.out.println("isNew: " + session.isNew());
			
			// even if sessin is not null check if it has the required user attributes
			// which will confirm whether or not the user has logged in. If user hasn't 
			// logged in, then redirect to login page
			
			pathInfo = req.getPathInfo();
			if("/submitlogin".equals(pathInfo)){
				String userId = req.getParameter("userId");
				String password = req.getParameter("password");
				
				if("osmand".equals(userId) && "test123".equals(password)){
					session.invalidate();
					session = req.getSession();
					System.out.println("\n\n===========> New HttpSession: " + session);
					System.out.println("getId: " + session.getId());
					System.out.println("getLastAccessedTime: " + session.getLastAccessedTime());
					System.out.println("getMaxInactiveInterval: " + session.getMaxInactiveInterval());
					System.out.println("isNew: " + session.isNew());				
					session.setAttribute("user", userId);
				}else {
					isBadLogin = true;
				}
			}
			
			user = (String)session.getAttribute("user");			
			
		
		}else {
			System.out.println("HttpSession is null and login is required..");
		}
		
		System.out.println("getRemoteUser: " + req.getRemoteUser());
		System.out.println("getRemoteAddr: " + req.getRemoteAddr());
		System.out.println("getRemoteHost: " + req.getRemoteHost());
				
		
		if(user == null && !isBadLogin){
			// redirect to login page
			resp.sendRedirect(req.getContextPath() +  "/login.jsp");
		
		} else if(isBadLogin){
			// redirect to badlogin page
			resp.sendRedirect(req.getContextPath() +  "/badlogin.jsp");
		
		}
		else {
		
			filterChain.doFilter(req, resp);
	        long end = System.currentTimeMillis();
	        System.out.println("Request processing time in seconds: " + (Double.valueOf((end - start)/1000)));
		}
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		System.out.println("in init() ...");
		
	}

}
