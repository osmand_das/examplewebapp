package com.osmand.example.webapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ControllerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		System.out.println("===========> in doPost ...");
		
		String user = null;
		String pathInfo = req.getPathInfo();		
		HttpSession session = req.getSession(false);
		
		System.out.println("===========> HttpSession: " + session);
		
		if(session != null) {
			user = (String)session.getAttribute("user");		
		}else {
			System.out.println("HttpSession is null and login is required..");
		}
		
		System.out.println("\n\n");
		
		if(user == null){
			// redirect to login page
			resp.sendRedirect(req.getContextPath() +  "/login.jsp");
			
		} else if ("/submitlogin".equals(pathInfo)) {
			System.out.println("forwarding request to home page");
			req.getRequestDispatcher("/index.jsp").forward(req, resp);
			
		}else if ("/login".equals(pathInfo)) {
			session.invalidate();
			System.out.println("invalidated existing session, now redirecting to login page");
			resp.sendRedirect(req.getContextPath() +  "/login.jsp");
		}
		else {
			System.out.println("forwarding request to following jsp: " + pathInfo);
			
			// Code to produce a OutOfMemoryError
			/*long count = 1999999999;
			List mylist = new ArrayList();
			int counter = 0;
			System.out.println("Going to print long array.... " );
			for(long i=0; i<= count; ++ count){
				Map myMap = new HashMap();
				myMap.put("name", "Osmand");
				myMap.put("address", "Pune, India");
				myMap.put("service", "Java");
				myMap.put("education", "MTech");
				mylist.add(myMap );
				counter++;
				if(counter%100000 == 0){
					System.out.println("Counter: " + counter);
				}
			}
			System.out.println("long array:" + mylist);*/
			req.getRequestDispatcher("/WEB-INF/jsp" + pathInfo + ".jsp").forward(req, resp);
		}
		
		
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("===========> in doGet ...");
		doPost(req, resp);
	}
	
	

}
