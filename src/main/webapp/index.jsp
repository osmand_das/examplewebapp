<html>
<body>
<h2>Home Page</h2>

<% 
	boolean loggedInUser = false;
	HttpSession userSession = request.getSession(false);
	if(userSession != null){
		out.print("HttpSession is not Null..");
		if(userSession.getAttribute("user")!= null){
			loggedInUser = true;
		}
	}else {
		out.print("===> HttpSession is Null..");
	}
%>

<% if(!loggedInUser) { %>
	<a href="<%= request.getContextPath() + "/control/login" %>">Login</a> to access details.
<% } else { %>
	<a href="<%= request.getContextPath() + "/control/details" %>">Details</a> to access details.
<% } %>
</body>
</html>
